﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="StudyGuide._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>The main purpose of this guide is to make available key concepts of JavaScript, Database and .NET on one platform. Each page  consists of 4 modules. First module explains some tricky concepts of the subject. Second module contains snippet of code which I wrote. Third module contains some code snippets I found on internet and which can be of some use. The fourth module contains links for further study.</p>
            <p>Nooooo, I was just joking.....The real purpose is to get some good grades in this assignment from Christine</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <a href="Javascript.aspx"><img src="images/js.png"></a>
        </div>
        <div class="col-md-4">
            <a href="Database.aspx"><img src="images/db.png"></a>
        </div>
        <div class="col-md-4">
            <a href="Net.aspx"><img src="images/nt.png"></a>
        </div>
    </div>

</asp:Content>
