﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StudyGuide
{
    public partial class Javascript : System.Web.UI.Page
    {
        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();
            DataColumn idx_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            List<string> front_end_code = new List<string>(new string[]{
            "while (repeat)",
            "{",
             "~var userInput = prompt('Which Top 10 book would you like', 'Pick a number:1-10');",
             "~if (userInput == null || userInput == '')",
             "~{",
             "~~alert('Please enter a number between 1 and 10!');",
             "~}",
             "~else",
             "~{",
             "~~alert('Number'+ userInput + ' on the list is ' + bestBooks[userInput - 1]);",
             "~~repeat = false;",
             "~}",
             "}"
             });
            int i = 0;
            foreach (string code_line in front_end_code)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;
                i++;
                codedata.Rows.Add(coderow);
            }
            DataView codeview = new DataView(codedata);
            return codeview;
        }
        DataView CreateCodeSource1()
        {
            DataTable codedata = new DataTable();
            DataColumn idx_col = new DataColumn();
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            List<string> front_end_code = new List<string>(new string[]{
            "function Counter()",
            "{",
             "~this.num = 0;",
             "~this.timer = setInterval(function add() {",
             "~~this.num++;",
             "~~console.log(this.num);",
             "~}, 1000);",
            "}"
            });

            int i = 0;
            foreach (string code_line in front_end_code)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;
                i++;
                codedata.Rows.Add(coderow);
            }
            DataView codeview = new DataView(codedata);
            return codeview;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();
            DataView ds1 = CreateCodeSource1();

            Code_MyCode.DataSource = ds;
            Code_FoundCode.DataSource = ds1;

            Code_MyCode.DataBind();
            Code_FoundCode.DataBind();
        }
    }
}