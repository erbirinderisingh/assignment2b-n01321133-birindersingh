﻿<%@ Page Title="Database" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="StudyGuide.Database" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <img src="images/db.png">
            <h2>Study Guide for Database</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Tricky Concept</h3>
                </div>
                <div class="panel-body">
                <p>SQL join is a Structured Query Language (SQL) instruction to combine data from two sets of data (i.e. two tables). Before we dive into the details of a SQL join, let’s briefly discuss what SQL is, and why someone would want to perform a SQL join.</p>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Code Snippet</h3>
                </div>
                <div class="panel-body">
                    <asp:DataGrid ID="Code_MyCode" runat="server" CssClass="code" GridLines="None" Width="100%" CellPadding="1" >
                    <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
                    <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
                    </asp:DataGrid>
                    <!--
                    <pre>
SELECT vendors.vendor_name,COUNT(invoices.invoice_id) AS "TOTAL INVOICES" 
FROM vendors FULL JOIN invoices
ON vendors.vendor_id=invoices.vendor_id
GROUP BY invoices.vendor_id,vendors.vendor_name
HAVING COUNT(invoices.invoice_id)=0
ORDER BY vendor_name ASC</pre>
                    -->
                </div>
            </div>
        </div>
     </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Found this</h3>
                </div>
                <div class="panel-body">
                    <p>
                        What if we want to run aggregate functions on more than one thing? The GROUP BY clause lets us run a function on each distinct value in the column that we group by.
                    </p>
                      <asp:DataGrid ID="Code_FoundCode" runat="server" CssClass="code" GridLines="None" Width="100%" CellPadding="1" >
                    <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
                    <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
                    </asp:DataGrid>
                    <!--
                    <pre>
SELECT primary_class, ROUND(AVG(intelligence), 3) as smarts
FROM characters
GROUP BY primary_class
ORDER BY smarts DESC
                    </pre>
                    -->
                    <em>Ref:https://simonborer.github.io/db-course/notes/week-3/#having</em>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Further Reading</h3>
                </div>
                <div class="panel-body">
                    <ul>
                        <li>https://simonborer.github.io/db-course/notes/week-3/#having</li>
                        <li>https://learn.humber.ca</li>
                    </ul>
                </div>
            </div>
        </div>
     </div>
</asp:Content>
