﻿<%@ Page Title="JavaScript" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Javascript.aspx.cs" Inherits="StudyGuide.Javascript" %>
<asp:Content ID="JavaScriptContent" ContentPlaceHolderID="MainContent" runat="server">
     <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <img src="images/js.png">
            <h2>Study Guide for Javascript</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Tricky Concept</h3>
                </div>
                <div class="panel-body">
                    <p>An array is a special variable, which can hold more than one value at a time. If you have a list of items (a list of car names, for example), storing the cars in single variables could look like this:
                    <pre>
                        var car1 = "Saab";
                        var car2 = "Volvo";
                        var car3 = "BMW";
                    </pre>
                    <p>However, what if you want to loop through the cars and find a specific one? And what if you had not 3 cars, but 300? The solution is an array! 
                     An array can hold many values under a single name, and you can access the values by referring to an index number.</p>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Code Snippet</h3>
                </div>
                <div class="panel-body">
                    <asp:DataGrid ID="Code_MyCode" runat="server" CssClass="code" GridLines="None" Width="100%" CellPadding="1" >
                    <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
                    <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
                    </asp:DataGrid>
                    <!--
                    <pre>
                            while(repeat)
                            {
                            var userInput=prompt("Which Top 10 book would you like","Pick a number:1-10");
                            if(userInput==null || userInput=="") {
	                            alert("Please enter a number between 1 and 10!");
                            }
                            else{
                                alert("Number "+userInput+ " on the list is "+bestBooks[userInput-1]);
	                            repeat=false;                              
                            }
                            }
                    </pre>
                    -->
                </div>
            </div>
        </div>
     </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">Found this</h3>
                </div>
                <div class="panel-body">
                    <p>What if we want to increase the value of a.num every second? We can use the setInterval() function. setInterval() is a function that calls another function after a set number of milliseconds. Let’s add it to our Counter function:</p>
                    <asp:DataGrid ID="Code_FoundCode" runat="server" CssClass="code" GridLines="None" Width="100%" CellPadding="1" >
                    <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
                    <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
                    </asp:DataGrid>
                    <!--
                    <pre>
                        function Counter() {
                        this.num = 0;
                        this.timer = setInterval(function add() {
                        this.num++;
                        console.log(this.num);
                        }, 1000);
                        }
                   </pre>
                    -->
                    <em>Ref:https://codeburst.io/javascript-arrow-functions-for-beginners-926947fc0cdc</em>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Further Reading</h3>
                </div>
                <div class="panel-body">
                    <ul>
                        <li>https://codeburst.io/javascript-arrow-functions-for-beginners-926947fc0cdc</li>
                        <li>https://w3schools.com</li>
                        <li>https://www.youtube.com</li>
                    </ul>
                </div>
            </div>
        </div>
     </div>
</asp:Content>
